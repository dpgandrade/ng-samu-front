﻿import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { LoginService } from './login.service';

@Component({
    selector:    'app-login',
    templateUrl: './login.template.html',
    styleUrls:  ['./login.template.css'],
    providers:  [LoginService]
})

export class LoginComponent implements OnInit {
    loading: boolean = false;
    erro:    boolean = false;
    erroMsg: string  = '';

    constructor(
        private router: Router,
        private loginService: LoginService) {
    }

    ngOnInit() {
      this.loginService.logout();
    }

    login(event, lgn, senha) {
       event.preventDefault();
       this.loading = true;
       this.loginService.login(lgn,senha)
            .subscribe(result => {
                if (result === 200) {
                  window.location.href = '/pessoa';
                } else {
                    this.erro    = true;
                    this.erroMsg = result.mensagem;
                    this.loading = false;
                }
            });
    }
}
