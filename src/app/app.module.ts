import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule, Http, RequestOptions } from '@angular/http';

import { BasicModule }    from './app-basic.module';
import { AppComponent }   from './app.component';
import { routing }	      from './app.routing';
import { AuthGuard }      from './common/auth.guard';

import { LoginComponent } from './login/login.component';
import { LoginService }   from './login/login.service';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    BasicModule,
    routing
  ],
  providers: [AuthGuard, LoginService],
  bootstrap: [AppComponent]
})
export class AppModule {}
