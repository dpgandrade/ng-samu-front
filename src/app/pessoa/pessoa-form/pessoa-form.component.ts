import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import { Subscription, Observable } from 'rxjs/Rx';

import {Message} from 'primeng/primeng';

import { PessoaService }          from '../pessoa.service';
import { Pessoa }                 from '../pessoa';
import { PessoaValidators }       from './pessoa-validators';
import { ComponentCanDeactivate } from '../pessoa.guard';

@Component({
  selector: 'app-pessoa-form',
  templateUrl: './pessoa-form.template.html',
  styleUrls: ['./pessoa-form.template.css']
})
export class PessoaFormComponent implements OnInit, OnDestroy, ComponentCanDeactivate {

  form: FormGroup;

  private pessoaIndex: number;
  private title: string;
  private isNew: boolean = true;
  private pessoa: Pessoa;
  private subscription: Subscription;
	private loading: boolean = false;
  private btnValue: string;

  msgs:    			     Message[] = [];
	alerts:						 Message[] = [];

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private pessoaService: PessoaService) { }

  ngOnInit() {
    this.subscription = this.route.params.subscribe(
      (params: any) => {
        if (params.hasOwnProperty('id')) {
          this.loading = true;
          this.title = 'Editar pessoa';
          this.isNew = false;
          this.pessoaIndex = +params['id'];
          this.pessoaService.get(this.pessoaIndex).
               subscribe(response =>  {
                 this.setPessoa(response);
                 this.loading = false;
                 this.btnValue = 'save';
               })
        } else {
          this.title = 'Nova pessoa';
          this.isNew = true;
          this.pessoa = new Pessoa();
          this.loading = false;
          this.btnValue = 'add';
        }
      }
    );
    this.initForm();
  }

  private setPessoa(pessoa: Pessoa){
       this.pessoa = pessoa;
  }

  private initForm() {
    this.form = this.formBuilder.group({
      nome: ['', [
        Validators.required,
        Validators.minLength(5)
      ]],
      status: ['', [
        Validators.required
      ]],
      dtNascimento: ['', [ ]],
			codAux :['',[ ]],
			sexo: ['',[
				Validators.required
			]],
      email: ['', [
        Validators.required,
        PessoaValidators.email
      ]]
    });
  }

  onCancel() {
    this.navigateBack();
  }

  private navigateBack() {
    this.router.navigate(['/pessoa']);
  }

  onSave() {
		this.loading = true;
    let result, acao, dtNasc;
    const pessoaValue = this.form.value;

    dtNasc = pessoaValue['dtNascimento'].match(/(\d+)/g)
    pessoaValue['dtNascimento']  = new Date(dtNasc[0],dtNasc[1]-1,dtNasc[2]);


    if (this.isNew){
      acao   = ' inserida ';
      pessoaValue['status']  = 'A'; // status A no cadastro;
    } else {
      acao   = ' editada ';
      pessoaValue['id']      = this.pessoaIndex;
    }

    result = this.pessoaService.save(pessoaValue);

    this.form.reset();

    result.subscribe(data => {
      this.loading = false;
      this.showAlert({
        severity:'success',
        summary:'Pessoa '+ acao +' com sucesso',
        detail:''
      });
    },
    err => {
      alert("Um erro ocorreu");
      console.log(err);
    },
    () => this.navigateBack());
  }

  inativar(pessoa: Pessoa){
    this.msgs.push({
      severity:'warn',
      summary:pessoa.nome+ ' será inativado(a) ',
      detail:'Ele(a) não aparecerá nas principais listas'
    });
  }

  showAlert(msgs: any) {
    this.alerts.push(msgs);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  canDeactivate(): Observable<boolean> | boolean {
    if (this.form.dirty) {
      return confirm('Deseja mesma sair dessa página ?');
    }
    return true;
  }
}
