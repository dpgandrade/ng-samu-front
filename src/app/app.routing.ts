import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { AuthGuard }      from './common/auth.guard';

const appRoutes: Routes = [
	{path: 'pessoa',
	 loadChildren: 'app/pessoa/pessoa.module#PessoaModule',
	 canActivate: [AuthGuard]},

	{path: '', 			 component: LoginComponent, },
	{path: 'login',  component: LoginComponent, canActivate:[AuthGuard]},
	{path: 'logout', component: LoginComponent},

	{path: '**',  	 redirectTo: 'login'}
];


export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
