import { Component, OnInit, Input } from '@angular/core';

import { Pessoa } from '../../interfaces/index';

@Component({
  selector: 'app-pessoa-list-item',
  templateUrl: '../../templates/Pessoa/pessoa-list-item.template.html',
  styleUrls: ['../../templates/Pessoa/pessoa-list-item.template.scss']
})
export class PessoaListItemComponent implements OnInit {

  @Input() pessoa: Pessoa;

  constructor() { }

  ngOnInit() {
  }

}
