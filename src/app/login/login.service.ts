﻿import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';

import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'

import { contentHeaders } from '../common/headers';

@Injectable()
export class LoginService {
    public token: string;
    private url = 'http://localhost:8789/samu/usuario/login';

    constructor(private http: Http) {
        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.token = currentUser && currentUser.token;
    }

    login(login: string, senha: string) :Observable <any>{
	    return this.http.post(this.url, JSON.stringify({login: login, senha: senha}), {headers:contentHeaders})
            .map((response: Response) => {
                // reposta jwt token
                let token = response.json() && response.json().token;
                if (token) {
                    this.token = token;

                    localStorage.setItem('token',token);
                    localStorage.setItem('user_id',  response.json().user_id);
                    localStorage.setItem('nivel_id', response.json().nivel_id);
                    localStorage.setItem('pessoa_id',response.json().pessoa_id);

                    return response.status;
                } else {
                    return response.json();
                }
            });
    }


    logout(): void {
        this.token = null;
        localStorage.removeItem('currentUser');
    }
}
