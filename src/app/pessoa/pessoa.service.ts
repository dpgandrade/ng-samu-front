import { Injectable, EventEmitter }    from '@angular/core';
import { Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Rx';

import { contentHeaders } from '../common/headers';
import { Pessoa } from './pessoa';

@Injectable()
export class PessoaService {
  private pessoa: Pessoa;

  private url: string = 'http://localhost:8789/samu/pessoa';
  pessoasModificadas = new EventEmitter<Observable <Pessoa[]>>();

  constructor(private http: Http) { }

    getAll(): Observable<Pessoa[]> {
      return this.http.get(this.url+ '/getAll')
                 .map(response =>
                   response.json().pessoa as Pessoa[]
                  )
                 .catch(this.handleError);
    }

    get(id): Observable <Pessoa>{
       return this.http.get(this.url+ '/get/' +id)
                 .map(response => response.json().pessoa as Pessoa)
                 .catch(this.handleError);
    }

    save(pessoa: Pessoa){
     return this.http.put(this.url+'/save', JSON.stringify(pessoa),
         {headers: contentHeaders})
       .map(res => res.json().data)
       .do(data => this.pessoasModificadas.emit(this.getAll()))
       .catch(this.handleError);
   }

   delete(id: number): Promise<void> {
     return this.http.delete(this.url+'/delete/'+id, {headers: contentHeaders})
       .toPromise()
       .then(() => null)
       .catch(this.handleError);
   }

   private getUrl(id){
     return `${this.url}/${id}`;
   }

   /* Relatorios e Graficos */

   getPessoasGenero(): Observable <any>{
     return this.http.get(this.url+ '/getPessoasBySexo', {headers: contentHeaders})
               .map(response => response.json() )
               .catch(this.handleError);
   }

   /* Versão antiga, remover após testar*/

    getPessoas(): Promise<any> {
      return this.http.get(this.url+ '/getAll')
                 .toPromise()
                 .then(response =>response.json())
                 .catch(this.handleError);
    }


  getPessoa(id: number): Promise<Pessoa> {
    const url = `${this.url}/${id}`;
    return this.http.get(url)
      .toPromise()
      .then(response => response.json().data as Pessoa)
      .catch(this.handleError);

  }

  create(name: string): Promise<Pessoa> {
    return this.http
      .post(this.url, JSON.stringify({name: name}), {headers: contentHeaders})
      .toPromise()
      .then(res => res.json().data)
      .catch(this.handleError);
  }

  oldUpdate(pessoa: Pessoa): Promise<Pessoa> {
    const url = `${this.url}/${pessoa.id}`;
    return this.http
      .put(url, JSON.stringify(pessoa), {headers: contentHeaders})
      .toPromise()
      .then(() => pessoa)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('Ocorreu um erro', error);
    return Promise.reject(error.message || error);
  }
}
