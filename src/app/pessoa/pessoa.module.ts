import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule }  from '@angular/router';
import { HttpModule }  from '@angular/http';

import { MessagesModule, GrowlModule} from 'primeng/primeng';
import { ConfirmDialogModule, ConfirmationService } from 'primeng/primeng';
import { InputTextModule } from 'primeng/primeng';
import { PasswordModule } from 'primeng/primeng';
import { ButtonModule } from 'primeng/primeng';
import { DataTableModule, DropdownModule } from 'primeng/primeng';
import { DialogModule } from 'primeng/primeng';
import { MegaMenuModule} from 'primeng/primeng';
import { ChartModule } from 'primeng/primeng';

import {  PessoaComponent }          from './pessoa.component';
import {  PessoaDashBoardComponent } from './pessoa-dashboard/pessoa-dashboard.component';
import {  PessoaFormComponent }      from './pessoa-form/pessoa-form.component';
import {  pessoasRouting }           from './pessoa.routing';
import {  PessoaService }            from './pessoa.service';

@NgModule({
    imports: [
      CommonModule,
      FormsModule,
      ReactiveFormsModule,
      RouterModule,
      HttpModule ,

      MessagesModule,
      GrowlModule,
      ConfirmDialogModule,
      DataTableModule,
      DropdownModule,
      ChartModule,

      pessoasRouting
    ],
    declarations: [
      PessoaComponent,
      PessoaDashBoardComponent,
      PessoaFormComponent
    ],
    providers: [
       PessoaService, ConfirmationService
    ]
})
export class PessoaModule {}
