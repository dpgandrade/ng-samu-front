export interface Menu {
  id:		       number;
  nome:		     string;
  link:		     string;
  ordem:       number;

  menu_pai_id: number;
}
