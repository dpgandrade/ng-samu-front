import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { tokenNotExpired } from 'angular2-jwt';

@Injectable()
export class AuthGuard implements CanActivate {
  autenticado: boolean = false;

  constructor(private router: Router) {
  }

  canActivate() {
    if (tokenNotExpired(localStorage.getItem('user_id'), localStorage.getItem('token'))) {
       this.autenticado = true;
       return true;
    }
    return false;
  }

  /*
  getPessoaAuth(){
    this.pessoaService.get(localStorage.getItem('pessoa_id'))
                      .subscribe(response => {
                        this.setPessoaAuth(response);
                        this.autenticado = true;
                      });
  }
  */

  logout() {
    localStorage.clear();
    this.router.navigate(['/login']);
  }
  /*
  setPessoaAuth(pessoa: Pessoa){
    this.pessoaAuth = pessoa;
  }
  */

}
