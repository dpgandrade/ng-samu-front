import { Component, OnInit } from '@angular/core';

import { PessoaService } from '../../service/index';
import { Pessoa } from '../../interfaces/index';

@Component({
  selector: 'app-pessoa-list',
  templateUrl: '../../templates/Pessoa/pessoa-list.template.html',
  styleUrls: ['../../templates/Pessoa/pessoa-list.template.scss']
})
export class PessoaListComponent implements OnInit {

  pessoas: Pessoa [] = [];

  constructor(private pessoaService: PessoaService) { }

    getPessoas(): void {
  		this.pessoaService
  		    .getAll()
  		    .subscribe(pessoas => this.pessoas = pessoas, err =>{
            console.log('houve um erro em PessoaListComponent > getPessoas');
          });
  	}

    pessoasModificadas(){
      this.pessoaService.pessoasModificadas
          .subscribe((observable: any) =>observable.subscribe(
            data => this.pessoas = data
          ))
    }

    ngOnInit() {
      this.getPessoas();
    }

}
