import{ Component, EventEmitter } from '@angular/core';

import { MaterializeDirective, MaterializeAction } from 'angular2-materialize';

import{ AuthGuard }    from '../common/auth.guard';
import{ PessoaService} from '../pessoa/pessoa.service';
import{ Pessoa }       from '../pessoa/pessoa';

declare var jQuery:any;

@Component({
    selector:    'app-menu',
    templateUrl: './menu.template.html',
    styleUrls:  ['./menu.template.css']
})

export class MenuComponent {

  private showMenu: boolean = false;
  private login: any;

  constructor(private authGuard: AuthGuard,
              private pessoaService: PessoaService){
     this.login = authGuard.autenticado;
  }

  ngOnInit() {
    jQuery('.button-collapse').sideNav();

    if(this.isAuth()){
      this.pessoaService.get(localStorage.getItem('pessoa_id'))
                        .subscribe(response => {
                          this.setUsuario(response);
                          this.showMenu = true;
                        });
    }
  }

  isAuth(){
    return this.authGuard.canActivate();
  }

  setUsuario(pessoa: Pessoa){
    this.login = pessoa;
  }

  logout(){
    return this.authGuard.logout();
  }
}
