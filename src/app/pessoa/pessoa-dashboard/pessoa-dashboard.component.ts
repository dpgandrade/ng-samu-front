import { Component, OnInit } from '@angular/core';
import { Router }            from '@angular/router';
import { AuthHttp } 			   from 'angular2-jwt';

import { Message } 						 from 'primeng/primeng';
import { ConfirmationService } from 'primeng/primeng';

import { Pessoa }        from '../pessoa';
import { PessoaService } from '../pessoa.service';

@Component({
	selector: 'app-pessoa-dashboard',
	templateUrl: './pessoa-dashboard.template.html',
  styleUrls: [ './pessoa-dashboard.template.css']
})
export class PessoaDashBoardComponent  implements OnInit{
  tituloPagina:      string    = 'Pessoas - visão geral';
	totalPessoas: 		 number    = 0;
	tituloGrafico: 		 string;
	generos: 					 any       = [];
	pessoas: 		  		 Pessoa[]  = [];
	pessoaSelecionada: Pessoa;
  msgs:    			     Message[] = [];
	alerts:						 Message[] = [];
  data: 						 any;
	loading:           boolean   = false;
	msgDeletePessoa:   boolean   = false;

	constructor(private pessoaService:  		 PessoaService,
							private confirmationService: ConfirmationService,
		    			private router:	   		 			 Router){

				pessoaService.getPessoasGenero()
											.subscribe(response => {
												this.loading = true;
												this.grafPessoaGenero(response.pessoas);
												this.setGeneros();
												this.loading = false;
											});
	}

	getPessoas(): void {
		this.loading = true;
		this.pessoaService
		    .getPessoas()
		    .then(result => {
					this.pessoas      = result.pessoas as Pessoa[];
					this.totalPessoas = result.total;
					this.loading      = false;
				});
	}

	deleteConfirm(pessoa: Pessoa): void {
		this.confirmationService.confirm({
					message: 'Confirma  exclusão de ' +pessoa.nome+ '?',
					accept: () => {
							this.delete(pessoa);
					}
			});
	}

	delete(pessoa: Pessoa): void{
		this.loading = true;
		this.pessoaService
		.delete(pessoa.id)
		.then(() => {
		  this.pessoas = this.pessoas.filter(h => h !== pessoa);
		  if (this.pessoaSelecionada === pessoa) {
				this.pessoaSelecionada = null;
			}
			this.loading = false;
			this.showAlert({severity:'success',
										summary:pessoa.nome+' foi excluída com sucesso',
									  detail:''});
		});

	}

	setGeneros(){
		this.generos.push({label:'Todos', value:null});
		this.generos.push({label:'Masculino', value:'M'});
		this.generos.push({label:'Feminino', value:'F'});
	}

	/* construindo grafico */

	grafPessoaGenero(dados: any){
		 this.tituloGrafico = 'Pessoa por gênero';

		 let labels = [];
		 let data   = [];

		 for(let dado of dados){
			 labels.push(dado.genero);
			 data.push(dado.count);
		 }
		 this.data = {
					 labels: labels,
					 datasets: [
							 {
									 data: data,
									 backgroundColor: [
											 "#FF6384",
											 "#36A2EB"
									 ],
									 hoverBackgroundColor: [
											 "#FF6384",
											 "#36A2EB"
									 ]
							 }]
					 };
	}

	showAlert(msg: any) {
        this.alerts = [];
        this.alerts.push(msg);
  }

	ngOnInit(): void {
	  this.getPessoas();
	}

}
