export class Pessoa {
  id:                 number;
  nome:               string;
  sexo:	              string;
  status:	            string;
  dtNascimento:       Date;
  imagem:	            string;
  rg:		              string;
  cpf:		            string;
  telefoneFixo:       string;
  celular:            string;
  celularAdicional:  string;
  login:             string;
  email:             string;
  obs:               string;

  idGrupo:	         number;
  idProfissao:       number;
  codAux:            number;

  // (Engenharia reversa aqui?)
}
