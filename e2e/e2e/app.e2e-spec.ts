import { SFrontPage } from './app.po';

describe('s-front App', function() {
  let page: SFrontPage;

  beforeEach(() => {
    page = new SFrontPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
