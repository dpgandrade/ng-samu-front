import { Routes, RouterModule } from '@angular/router';

import {  PessoaComponent }          from './pessoa.component';
import {  PessoaDashBoardComponent } from './pessoa-dashboard/pessoa-dashboard.component';
import {  PessoaFormComponent }      from './pessoa-form/pessoa-form.component';
import {  PessoaGuard }          from './pessoa.guard';

const PESSOAS_ROUTES: Routes = [
  { path: '',
    component: PessoaComponent,
    children: [
        { path: '', component: PessoaDashBoardComponent },
        { path: 'add', component: PessoaFormComponent /*, canDeactivate: [PessoaFormGuard]*/ }, // canDeactivate impede o cancelar involutario do form...  porém nao ta funfa.. :(
        { path: ':id', component: PessoaFormComponent}
  ]}
];

/* Por ser <const>, o nome do router tem inicial minuscula */
export const pessoasRouting = RouterModule.forChild(PESSOAS_ROUTES);
