import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule }  from '@angular/router';

import { MaterializeModule } from "angular2-materialize";

import { MenuComponent } from './menu/menu.component';
import { PessoaService } from './pessoa/pessoa.service';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    MaterializeModule
  ],
  declarations: [
    MenuComponent
  ],
  exports: [
    MenuComponent
  ],
  providers: [
    PessoaService
  ]
})
export class BasicModule {}
