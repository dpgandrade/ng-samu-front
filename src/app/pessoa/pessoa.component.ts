import { Component, OnInit } from '@angular/core';
import { Router }            from '@angular/router';
import { AuthHttp } 			   from 'angular2-jwt';

import { Pessoa }        from './pessoa';
import { PessoaService } from './pessoa.service';

@Component({
	selector: 'app-pessoa',
	templateUrl: './pessoa.template.html'
})
export class PessoaComponent  implements OnInit{
  constructor(){}

	ngOnInit(){}

}
